package com.example.upa_xipplg2_3025

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val activityBiodata = findViewById<Button>(R.id.buttonbiodata)
        activityBiodata.setOnClickListener {
            val intent = Intent(this,Activity_Biodata::class.java)
            startActivity(intent)
        }
        val activityPendidikan= findViewById<Button>(R.id.buttonpendidikan)
        activityPendidikan.setOnClickListener {
            val intent = Intent(this,Activity_Pendidikan::class.java)
            startActivity(intent)
        }
        val activityPortofolio= findViewById<Button>(R.id.buttonportofolio)
        activityPortofolio.setOnClickListener {
            val intent = Intent(this,Activity_Portofolio::class.java)
            startActivity(intent)

        }
    }

}